
Last node modification

This module simply creates a block that can be shown at any region.
It hooks into the node saving functions and saves the date of the last
modification of any node in the system. That date is then shown in the
mentioned block.

Please notice that there is absolutely no additional database request,
so adding this module does not increase loading time by counting nodes and 
comparing their date to the current one at every click. It only fetches ONE 
variable that was saved at node createion/modification time. So it is 
considered very fast.




There are plenty of things TODO:
 * Make the string that is put out configurable
 * Use specific time zone settings?
 * Use another date format (ATM there is only the standard format available)
 * translation
 
 Any bug fixes, enhancements & patches welcome.
